from django.conf.urls import patterns, url, include

from djangular.views import IndexView

from rest_framework_nested import routers

from authentication.views import AccountViewSet, LoginView, LogoutView
from creations.views import AccountCreationsViewSet, CreationViewSet

router = routers.SimpleRouter()
router.register(r'accounts', AccountViewSet)
router.register(r'creations', CreationViewSet)

accounts_router = routers.NestedSimpleRouter(router, r'accounts', lookup='account')
accounts_router.register(r'creations', AccountCreationsViewSet)

urlpatterns = patterns(
    '',
    # ... URLs
    url(r'^api/v1/', include(router.urls)),
    url(r'^api/v1/auth/login/$', LoginView.as_view(), name='login'),
    url(r'^api/v1/auth/logout/$', LogoutView.as_view(), name='logout'),
    url(r'^api/v1/', include(router.urls)),
    url(r'^api/v1/', include(accounts_router.urls)),

    url('^.*$', IndexView.as_view(), name='index'),
)
