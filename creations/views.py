from rest_framework import permissions, viewsets
from rest_framework.response import Response

from creations.models import Creation
from creations.permissions import IsCreator
from creations.serializers import CreationSerializer


class CreationViewSet(viewsets.ModelViewSet):
    queryset = Creation.objects.order_by('-created')
    serializer_class = CreationSerializer

    def get_permissions(self):
        if self.request.method in permissions.SAFE_METHODS:
            return (permissions.AllowAny(),)
        return (permissions.IsAuthenticated(), IsCreator(),)

    def perform_create(self, serializer):
        instance = serializer.save(creator=self.request.user)

        return super(CreationViewSet, self).perform_create(serializer)


class AccountCreationsViewSet(viewsets.ViewSet):
    queryset = Creation.objects.select_related('creator').all()
    serializer_class = CreationSerializer

    def list(self, request, account_username=None):
        queryset = self.queryset.filter(creator__username=account_username)
        serializer = self.serializer_class(queryset, many=True)

        return Response(serializer.data)
