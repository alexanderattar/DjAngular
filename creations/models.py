from django.db import models

from authentication.models import Account


class Creation(models.Model):
    creator = models.ForeignKey(Account)
    description = models.TextField()

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return '{0}'.format(self.description)
