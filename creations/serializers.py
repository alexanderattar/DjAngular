from rest_framework import serializers

from authentication.serializers import AccountSerializer
from creations.models import Creation


class CreationSerializer(serializers.ModelSerializer):
    creator = AccountSerializer(read_only=True, required=False)

    class Meta:
        model = Creation

        fields = ('id', 'creator', 'description', 'created', 'modified')
        read_only_fields = ('id', 'created', 'modified')

    def get_validation_exclusions(self, *args, **kwargs):
        exclusions = super(CreationSerializer, self).get_validation_exclusions()

        return exclusions + ['creator']
