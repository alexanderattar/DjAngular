/**
* Creation
* @namespace thinkster.creations.directives
*/
(function () {
  'use strict';

  angular
    .module('thinkster.creations.directives')
    .directive('creation', creation);

  /**
  * @namespace Post
  */
  function creation() {
    /**
    * @name directive
    * @desc The directive to be returned
    * @memberOf thinkster.creations.directives.Post
    */
    var directive = {
      restrict: 'E',
      scope: {
        creation: '='
      },
      templateUrl: '/static/templates/creations/creation.html'
    };

    return directive;
  }
})();