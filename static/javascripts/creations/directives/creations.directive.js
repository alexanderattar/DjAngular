/**
* Creations
* @namespace thinkster.creations.directives
*/
(function () {
  'use strict';

  angular
    .module('thinkster.creations.directives')
    .directive('creations', creations);

  /**
  * @namespace Creations
  */
  function creations() {
    /**
    * @name directive
    * @desc The directive to be returned
    * @memberOf thinkster.posts.directives.Posts
    */
    var directive = {
      controller: 'CreationsController',
      controllerAs: 'vm',
      restrict: 'E',
      scope: {
        creations: '='
      },
      templateUrl: '/static/templates/creations/creations.html'
    };

    return directive;
  }
})();