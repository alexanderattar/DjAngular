/**
* Creations
* @namespace thinkster.creations.services
*/
(function () {
  'use strict';

  angular
    .module('thinkster.creations.services')
    .factory('Creations', Creations);

  Creations.$inject = ['$http'];

  /**
  * @namespace Creations
  * @returns {Factory}
  */
  function Creations($http) {
    var Creations = {
      all: all,
      create: create,
      get: get
    };

    return Creations;

    ////////////////////

    /**
    * @name all
    * @desc Get all Creations
    * @returns {Promise}
    * @memberOf thinkster.creations.services.Creations
    */
    function all() {
      return $http.get('/api/v1/creations/');
    }


    /**
    * @name create
    * @desc Create a new Creation
    * @param {string} description The description of the new Creation
    * @returns {Promise}
    * @memberOf thinkster.creations.services.Creations
    */
    function create(description, creator_id) {
      return $http.post('/api/v1/creations/', {
        description: description,
        creator_id: creator_id
      });
    }

    /**
     * @name get
     * @desc Get the Creations of a given user
     * @param {string} username The username to get Creations for
     * @returns {Promise}
     * @memberOf thinkster.creations.services.Creations
     */
    function get(username) {
      return $http.get('/api/v1/accounts/' + username + '/creations/');
    }
  }
})();