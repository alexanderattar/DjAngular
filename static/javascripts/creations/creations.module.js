(function () {
  'use strict';

  angular
    .module('thinkster.creations', [
      'thinkster.creations.controllers',
      'thinkster.creations.directives',
      'thinkster.creations.services'
    ]);

  angular
    .module('thinkster.creations.controllers', []);

  angular
    .module('thinkster.creations.directives', ['ngDialog']);

  angular
    .module('thinkster.creations.services', []);
})();