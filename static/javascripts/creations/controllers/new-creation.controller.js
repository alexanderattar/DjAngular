/**
* NewCreationController
* @namespace thinkster.creations.controllers
*/
(function () {
  'use strict';

  angular
    .module('thinkster.creations.controllers')
    .controller('NewCreationController', NewCreationController);

  NewCreationController.$inject = ['$rootScope', '$scope', 'Authentication', 'Snackbar', 'Creations'];

  /**
  * @namespace NewCreationController
  */
  function NewCreationController($rootScope, $scope, Authentication, Snackbar, Creations) {
    var vm = this;

    vm.submit = submit;

    /**
    * @name submit
    * @desc Create a new Creation
    * @memberOf thinkster.creations.controllers.NewCreationController
    */
    function submit() {
      $rootScope.$broadcast('creation.created', {
        description: vm.description,
        creator: {
          username: Authentication.getAuthenticatedAccount().username
        }
      });

      $scope.closeThisDialog();

      Creations.create(vm.description, Authentication.getAuthenticatedAccount().id).then(createCreationSuccessFn, createCreationErrorFn);


      /**
      * @name createCreationSuccessFn
      * @desc Show snackbar with success message
      */
      function createCreationSuccessFn(data, status, headers, config) {
        Snackbar.show('Success! Creation created.');
      }


      /**
      * @name createCreationErrorFn
      * @desc Propogate error event and show snackbar with error message
      */
      function createCreationErrorFn(data, status, headers, config) {
        $rootScope.$broadcast('creation.created.error');
        Snackbar.error(data.error);
      }
    }
  }
})();